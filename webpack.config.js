/*
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        '_template': './userscripts/_template.js',
        'Asana_show-existence-of-description-in-task-overview': './userscripts/Asana_show-existence-of-description-in-task-overview.js',
        'GitHub_absolute-time-instead-of-relative-time': './userscripts/GitHub_absolute-time-instead-of-relative-time.js',
        'GitLab_absolute-time-instead-of-relative-time': './userscripts/GitLab_absolute-time-instead-of-relative-time.js',
        'GitLab_extended-names-for-mentions-in-text': './userscripts/GitLab_extended-names-for-mentions-in-text.js',
        'GitLab_new-issue-button-in-board-view': './userscripts/GitLab_new-issue-button-in-board-view.js',
        'GitLab_remove-welcome-column-in-new-boards': './userscripts/GitLab_remove-welcome-column-in-new-boards.js',
        'GitLab_add-epic-workflow-and-cross-mentioning': './userscripts/GitLab_add-epic-workflow-and-cross-mentioning.js'
    },
    output: {
        path: __dirname + '/userscripts/build/',
        filename: '[name].min.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ],
                        plugins: [
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-proposal-private-methods'
                        ]
                    }
                }
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map'
};
