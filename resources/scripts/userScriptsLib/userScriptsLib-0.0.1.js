/*
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/**
 * Um eine URL in regex Form zu speichern und darin die Variablen zu kennzeichnen, die später verwendet werden können sollen.
 */
export class urlRegex{
    regex;
    groups;

    constructor(regex, groups){
        this.regex = regex;
        this.groups = groups;
    }
}

/**
 * Presets for the `urlRegex` Class.
 * Staticly call one of the functions and you get a fully filled out `urlRegex` object.
 */
export class urlRegex_presets{

    /**
     * GitLab: <alles erlaubt>
     * @returns {urlRegex} has the following variables: "fullPathVariables"
     */
    static gitlabEverything() {
        // Regex Aufbau der URL, hier alles erlaubt:   https://gitlab.com/<fullPathVariables>
        //                                      z.B:   https://gitlab.com/christopher2007/userscripts/-/wikis/Home
        return new urlRegex(/^https:\/\/gitlab\.com(\/.*)?$/,
            ["fullPathVariables"]);
    }

    /**
     * GitLab: Issue Detail Page
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke", "issueId"
     */
    static gitlabIssueDetailPage(){
        // Regex Aufbau der URL, hier "Issue":  https://gitlab.com/<username>/<repository>/(-/)issues/<issueId>
        //                                z.B:  https://gitlab.com/christopher2007/userscripts/issues/42
        //                                z.B:  https://gitlab.com/christopher2007/userscripts/-/issues/42
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?issues\/(\d+)$/,
            ["username", "repository", "stroke", "issueId"]);
    }

    /**
     * GitLab: Issue List
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke"
     */
    static gitlabIssueList(){
        // Regex Aufbau der URL, hier "Issue List":  https://gitlab.com/<username>/<repository>/(-/)issues
        //                                     z.B:  https://gitlab.com/christopher2007/userscripts/issues
        //                                     z.B:  https://gitlab.com/christopher2007/userscripts/-/issues
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?issues$/,
            ["username", "repository", "stroke"]);
    }

    /**
     * GitLab: Issue Board
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke", "boardId"
     */
    static gitlabIssueBoard(){
        // Regex Aufbau der URL, hier "Issue Board":    https://gitlab.com/<username>/<repository>/(-/)boards/<boardId>
        //                                      z.B:    https://gitlab.com/christopher2007/userscripts/boards/1103672
        //                                      z.B:    https://gitlab.com/christopher2007/userscripts/-/boards/1103672
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?boards\/(\d+)$/,
            ["username", "repository", "stroke", "boardId"]);
    }

    /**
     * GitLab: Wiki
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke", "wikiPageName"
     */
    static gitlabWiki(){
        // Regex Aufbau der URL, hier "Wiki":   https://gitlab.com/<username>/<repository>/(-/)wikis/<wikiPageName>
        //                               z.B:   https://gitlab.com/christopher2007/userscripts/wikis/Home
        //                               z.B:   https://gitlab.com/christopher2007/userscripts/-/wikis/Home
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?wikis\/([^\/]+)$/,
            ["username", "repository", "stroke", "wikiPageName"]);
    }

    /**
     * GitLab: Merge Request
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke", "mergeRequestId"
     */
    static gitlabMergeRequest(){
        // Regex Aufbau der URL, hier "Merge Request":  https://gitlab.com/<username>/<repository>/(-/)merge_requests/<mergeRequestId>
        //                                        z.B:  https://gitlab.com/christopher2007/userscripts/merge_requests/11
        //                                        z.B:  https://gitlab.com/christopher2007/userscripts/-/merge_requests/11
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?merge_requests\/(\d+)$/,
            ["username", "repository", "stroke", "mergeRequestId"]);
    }

    /**
     * GitLab: Milestone
     * @returns {urlRegex} has the following variables: "username", "repository", "stroke", "milestoneId"
     */
    static gitlabMilestone(){
        // Regex Aufbau der URL, hier "Milestone":  https://gitlab.com/<username>/<repository>/(-/)milestones/<milestoneId>
        //                                    z.B:  https://gitlab.com/christopher2007/userscripts/milestones/11
        //                                    z.B:  https://gitlab.com/christopher2007/userscripts/-/milestones/11
        return new urlRegex(/^https:\/\/gitlab\.com\/([^\/]+)\/([^\/]+)\/(-\/)?milestones\/(\d+)$/,
            ["username", "repository", "stroke", "milestoneId"]);
    }

}

/**
 * Sammelt URLs in regex Form für die das Skript verwendet werden soll. Splittet dabei auch die Variablen aus den URLs für spätere Verwendung.
 */
export class domainCheck{

    // Private Klassenvariablen
    #error = false; // Um zu cachen, ob es mindestens einen Fehler gab
    #domainAccepted = false; // ob die aktuelle URL im Browser auf eine der regex URLs geparst werden konnte
    #urlParameter = []; // zum Sammeln aller regex Gruppen

    /**
     * Konstruktor.
     *
     * @param allUrlsToUse urlRegex
     */
    constructor(allUrlsToUse){
        // Durch alle übergebenen URLs in Regex Form gehen
        for (let i = 0; i < allUrlsToUse.length; i++){
            // aktuelle URL im Browser auf die regex URL parsen und Variablen heraus speichern
            this.#splitUrlWithRegex(allUrlsToUse[i].regex, allUrlsToUse[i].groups);

            // Prüfen, ob die aktuelle URI der Seite im Browser auf diesen Regex gematcht werden kann
            if (allUrlsToUse[i].regex.test(location.href))
                this.#domainAccepted = true;
        }

        // Bei mindestens einem Fehler auf jeden Fall genau ein Popup anzeigen
        if(this.#error)
            alert("UserScript Error, please check console");
    }

    /**
     * Hilfsfunktion: Splittet die URL nach dem regex und speichert die Gruppen global ab
     *
     * @param regex
     * @param groupVariables
     */
    #splitUrlWithRegex(regex, groupVariables){
        // Die `groupVariables` müssen erweitert werden: Das erste Element muss ein leeres sein, daher alles um eins nach hinten schieben
        groupVariables.unshift("");

        // Nichts machen, wenn die URL im gesamten nicht auf den regex passt
        if(!location.href.match(regex))
            return;

        // Bricht die URL nach dem regex auf
        var splitted = regex.exec(location.href) || [];

        // Durch alle Gruppen gehen, die der Nutzer haben möchte
        for (const [key, value] of groupVariables.entries()) {
            // Gruppe überspringen, wenn der Nutzer keinen Namen angegeben hat
            if(value === undefined)
                continue;

            // Wenn es den Eintrag schon gibt, muss er exakt gleich sein
            if(this.#urlParameter[value] !== undefined && this.#urlParameter[value] === splitted[key])
                continue;

            // Ab hier darf es diesen Eintrag noch nicht geben, denn wenn doch, so ist er ungleich des schon ermittelten und gespeicherten Wertes
            if(this.#urlParameter[value] !== undefined){
                this.#error = true;
                console.log('%c [ERROR] regex group already used ', 'background: #fff; color: #ff0000;');
                continue;
            }

            // die Variable nur abspeichern, wenn es in der URL nicht leer gemappt wird
            if(splitted[key] !== undefined)
                this.#urlParameter[value] = splitted[key];
        }
    };

    /**
     * Gibt zurück, ob es Fehler in dem Aufbau des Objektes gab (Vorbereitung von Variablen, regex gruppen, ...)
     *
     * @returns {boolean}
     */
    hasError(){
        return this.#error;
    };

    /**
     * Gibt an, ob die aktuelle URL des Browsers mit eine der übergebenen regex URLs übereinstimmt. Wenn ja, soll das Skript für diese Domain
     * verwendet werden.
     *
     * @returns {boolean}
     */
    isDomainAccepted(){
        return this.#domainAccepted;
    };

    /**
     * Gibt eine ermittelte Variable aus der URL zurück.
     *
     * @param name String
     * @returns {*}
     */
    getVariable(name){
        return this.#urlParameter[name];
    }
}

// export function includeOnceFontAwesome4(){
//     // FontAwesome 4 einbinden (alt, aber online extern gehostet)
//     var fontAwesome4 = $('<link id="FontAwesome4" rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">');
//     fontAwesome4.insertAfter("body > *:last-child");
// }

/**
 * Fügt einen eigenen HTML Style Block an das Ende der aktuellen Seite an.
 *
 * @param style
 */
export function addOwnStyle(style){
    var newStyle = $(style);
    newStyle.insertAfter("body > *:last-child"); // (Es gibt von Greasemonkey/Tampermonkey einen `GM_addStyle(<cssString>);` Befehl, ich traue diesem
    // aber nicht so recht und da auch die IDE nicht so recht merkt, dass es sich da um CSS handelt, nutze ich lieber die jQuery Möglichkeit, auch
    // wenn sie langsamer sein könnte, da ja zuerst jQuery geladen werden muss.)
}

/**
 * Ruft jedes Mal, wenn ein ajax HTTP Request zum Server erkannt wird, die Callback Funktion auf. Snifft somit auf alle Kommunikationen zum
 * Server und reagiert auf diese.
 *
 * @param callbackFunction
 */
export function launchHttpRequestSniffer(callbackFunction){
    var proxied = window.XMLHttpRequest.prototype.send;
    window.XMLHttpRequest.prototype.send = function() {
        var pointer = this; // das Ajax Request Objekt selbst
        var intervalId = window.setInterval(function(){
            if(pointer.readyState !== 4)    // nur wenn die HTTP Anfrage fertig ist weiter machen
                                            // (4 bedeutet: alles lokal fertig, egal welche HTTP Statusmeldung beim Server ausgelöst wurde)
                return;
            clearInterval(intervalId);

            // Die übergebene Callback Funktion aufrufen und ihr das Ajax Request Objekt geben
            callbackFunction(pointer, arguments); // z.B.: pointer.responseText
        }, 100); // Wiederhole alle n Millisekunden, empfohlen ist `1`, finde ich aber etwas sehr häufig
        return proxied.apply(this, [].slice.call(arguments));
    };
}

/**
 * A helper class.
 * It takes the issue obj in json format that comes from the GitLab API when you request the details of an issue by its issue ID and extends it
 * with some more information (main epic issue or sub-epic issue, ...)
 */
export class issueObj{
    issueId;
    issueObj;
    isEpicMain;
    isEpicSub;
    epicLabel;
}

/**
 * This class lets you get information from GitLab (issues, labels, ...) by working with the official GitLab API. After the first fetch the data
 * is cached. So asking the same information multiple times, only the first return is a real-time information from GitLab. All following
 * answers are cached and may vary from the now e.g. changed data on the servers.
 */
export class GitLabApiWithCache{
    projectId; // integer, only the project id
    project = undefined;
    issueIdToIssueObj = []; // key=issueID, value=instance of `issueObj`
    labelToIssueObj = []; // key=epic label string, value=array of issueIds that belong to that epic
    epicLabelToMainEpicIssueObj = []; // key=epic label string, value=issueId that belong to that epic

    constructor(projectId) {
        this.projectId = projectId;
        this.constructorOwn(); //TODO bad because other functions can be calles without this one been ready
    }

    /**
     * own constructor because the real constructor can not be a `async` function.
     */
    constructorOwn = async function(){
        // fetch project information
        try{
            var projectInformation = await $.ajax({
                url: `https://gitlab.com/api/v4/projects/${this.projectId}/`,
                type: 'GET',
                datatype: 'json',
                // data: undefined,
            });
        }catch (e){
            alert('error fetching project description, please contact creator of the script.');
        }

        // cache project information
        this.project = projectInformation;
    };

    /**
     * Gets a issue object from an given issue ID.
     *
     * @param issueId The ID of the issue from the repository (small number), not the overall global GitLab issue ID (big number).
     * @returns issueObj A instance of the `issueObj` class.
     */
    getIssueObj = async function(issueId){
        // is this issue not yet cached?
        if(this.issueIdToIssueObj[issueId] === undefined){ // not yet cached, so use the GitLab API to get the information and cache them
            try{
                var issueTmp = await $.ajax({
                    url: `https://gitlab.com/api/v4/projects/${this.projectId}/issues/${issueId}`,
                    type: 'GET',
                    datatype: 'json',
                    // data: undefined,
                });
            }catch (e){
                alert('error fetching issue detail data from GitLab, please contact creator of the script.');
            }

            // check the labels of the issue and check if this issue is:
            // - a main epic issue
            // - a sub-epic issue
            // - nothing related to epic issues
            var issue_epicLabel = undefined;
            var issue_isEpicMain = false;
            var issue_isEpicSub = false;
            for (const elem of issueTmp.labels){
                if(elem == 'epic'){
                    issue_isEpicMain = true;
                }else if(elem.substr(0, 6) == 'epic::'){
                    if(issue_isEpicSub) // issue can only be part of one epic group
                        alert('This issue has more then one `epic::` labels. That is not allowed!');
                    issue_isEpicSub = true;
                    issue_epicLabel = elem.substr(6, elem.length);
                }
            }
            if(issue_isEpicMain) // If it is a main epic issue, it can be a sub-issue
                issue_isEpicSub = false;

            // cache the object for the next time
            var issue = new issueObj();
            issue.issueId = issueId;
            issue.issueObj = issueTmp;
            issue.epicLabel = issue_epicLabel;
            issue.isEpicMain = issue_isEpicMain;
            issue.isEpicSub = issue_isEpicSub;
            this.issueIdToIssueObj[issueId] = issue;
        }

        // return wanted object
        return this.issueIdToIssueObj[issueId];
    };

    /**
     * Takes an epic label String and returns a array of sub-epic issue objects that belong to that epic group.
     *
     * @param epicLabelString
     * @returns {Promise<void>}
     */
    getFromLabelAllIssueIds = async function(epicLabelString){
        // is this epic label not yet cached?
        if(this.labelToIssueObj[epicLabelString] === undefined) { // not yet cached, so use the GitLab API to get the information and cache it
            try{
                var tmp = await $.ajax({
                    // url: `https://gitlab.com/api/v4/projects/${this.projectId}/issues?labels=epic::${issue_epic}&state=opened`,
                    url: `https://gitlab.com/api/v4/projects/${this.projectId}/issues?labels=epic::${epicLabelString}`,
                    type: 'GET',
                    datatype: 'json',
                    // data: undefined,
                });
            }catch (e){
                alert('error fetching issue ids from label, please contact creator of the script.');
            }

            // cache the object for the next time
            var subIssues = [];
            for (const elem of tmp){
                subIssues.push(elem.iid);
            }
            this.labelToIssueObj[epicLabelString] = subIssues;
        }

        // // create tmp array with all objects in order to return them later. Therefore use the issueObj cache (get out of cache and if not there ask
        // // GitLab API online)
        // var tmp = [];
        // for (const issueId of this.labelToIssueObj[epicLabelString]){
        //     var a = await this.getIssueObj(issueId);
        //     tmp.push(a);
        // }
        //
        // return wanted objects
        // return tmp;

        // return wanted objects
        return this.labelToIssueObj[epicLabelString];
    };

    /**
     * Takes an epic label String and returns a single object of the main epic issue that belong to that epic group.
     *
     * @param epicLabelString
     * @returns {Promise<void>}
     */
    getFromEpicLabelMainEpicIssueId = async function(epicLabelString) {
        // is this epic label not yet cached?
        if(this.epicLabelToMainEpicIssueObj[epicLabelString] === undefined) { // not yet cached, so use the GitLab API to get the information and cache it
            try{
                var tmp = await $.ajax({
                    // url: `https://gitlab.com/api/v4/projects/${this.projectId}/issues?labels=epic,epic::${epicLabelString}&state=opened`,
                    url: `https://gitlab.com/api/v4/projects/${this.projectId}/issues?labels=epic,epic::${epicLabelString}`,
                    type: 'GET',
                    datatype: 'json',
                    // data: undefined,
                });
            }catch (e){
                alert('error fetching main epic issue from label - fetch error, please contact creator of the script.');
            }

            // length must be exactly 1
            if(tmp.length !== 1) // not enough or too many found
                alert('error fetching main epic issue from label - count error. There must be exactly one main epic issue, not less and nor more.');

            // cache the object for the next time
            this.epicLabelToMainEpicIssueObj[epicLabelString] = tmp[0].iid;
        }

        // return wanted objects
        return this.epicLabelToMainEpicIssueObj[epicLabelString];
    };

    /**
     * @param issueId
     * @returns {string}
     */
    getLinkToIssue = async function(issueId){
        var issueObj = await this.getIssueObj(issueId);
        var tmpIssue_active = (issueObj.issueObj.state == 'opened')?true:false;
        var tmpIssue_activeString = tmpIssue_active?'':' (closed)';
        var tmpIssue_activeStyle = tmpIssue_active?'':' style="text-decoration: line-through;"';
        return `<a href="${issueObj.issueObj.web_url}" i-thor-mentioning-extended="true">
                    <span>
                        <span ${tmpIssue_activeStyle}>
                            #${issueObj.issueObj.iid} ${issueObj.issueObj.title}
                        </span>
                        ${tmpIssue_activeString}
                    </span>
                </a>`;
    };

}
