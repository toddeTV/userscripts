/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            GitLab - extended names for mentions in text
// @name:en         GitLab - extended names for mentions in text
// @name:de         GitLab - erweiterte Namen bei Erwähnungen im Text
// @description     Sets automatically the name of a issue after it is mentioned with `#<number>` Tag in a issue text, a wiki page, a merge-request text and in comments. Extends also all `!<number>` mentionings of merge-requests with the merge-request name.
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_gitlab.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://gitlab.com/*
// @match           https://something.com/*
// @exclude         https://*.gitlab.com/*
// @exclude         https://blabla.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js
// @require         https://example.com/bla.js
// @require         https://example.com/something.js
//
// @resource        resourceName    http://www.example.com/example.png
// @resource        bliBlaBLo       http://www.example.com/bla.png
//
// @version         0.1.0
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_extended-names-for-mentions-in-text.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_extended-names-for-mentions-in-text.min.js
//
// @run-at          document-end
// @unwrap
// @noframes
//
// ==/UserScript==
*/

/*
    # Infos

    UserScript Doku:
    - https://www.tampermonkey.net/documentation.php
    - http://wiki.greasespot.net/API_reference
    - https://sourceforge.net/p/greasemonkey/wiki/GM_addStyle/

    Header Doku:
    - http://wiki.greasespot.net/Metadata_Block
    - https://sourceforge.net/p/greasemonkey/wiki/Metadata_Block/

    `@match` und `@include` sind so weit gleich, nur ist `@match` strikter und hat mehr Regeln, was ein `*` sein darf. Es ist somit sicherer. Außerdem
    ist `@match` schneller als `@include` und performanter. `@include` hingegen unterstützt Regex, was `@match` nicht tut.
    So ist ein
        @include         /^https?://example\.com/page\.php*key1=value1*{/         <- Das `{` muss hier weg, geht hier aber wegen des JavaScript Kommentars nicht
    equivalent zu
        @match           @match *://example.com/page.php*
        if (!/\bkey1=value1\b/.test (location.href))
            return;

    Die `@updateURL` wird nur aufgerufen um zu prüfen, ob die neue `@version` online neuer ist als die aktuell installierte. Ebenso wird überprüft,
    ob die online Datei neuer ist als die lokale Datei (Wirklich Datei Create Date, macht nur Greasemonkey nicht Tapermonkey).
    Die `@downloadURL` wird dann genutzt, um das Script wirklich herunter zu laden, wenn durch `@updateURL` eine neuere Version verifiziert wurde.
    Greasemonkey und Tapermonkey sind hier etwas unterschiedlich entwickelt und daher empfiehlt es sich, beide URL's einfach auf die selbe zu setzen.

    `@run-at` kann folgende Werte haben:
    - document-end (default) = The script will run after the main page is loaded, but before other resources (images, style sheets, etc.) have loaded.
      (The only guaranteed working value in Greasemonkey v4.x )
    - document-start = The script will run before any document begins loading, thus before any scripts run or images load.
    - document-idle = The script will run after the page and all resources (images, style sheets, etc.) are loaded and page scripts have run.

    Das `@unwrap` bewirkt, dass das Skript nicht gewrappt wird.
    Normalerweise wird ein Script automatisch von Greasemonkey als eine anonyme Funktion gewrappt, um Namenskollisionen vorzubeugen:
        (function(){ <<script source>> })();
    Mit `@unwrap` wird das Script von unten nicht gewrappt.
    -> Ich wrappe lieber selbst, um die Kontrolle zu haben, es auch in Teilen weg lassen zu können, also den Tag im Header drin lassen!

    Das `@noframes` bewirkt, dass das Script nur auf Top-Level ausgeführt wird und nicht in Frames auf der Ziel-Seite.
    -> Ich empfehle es WEG zu nehmen

    Ein `@resource` muss einen uniquen Namen haben.
    Bindet eine externe Resource zur Laufzeit in die Seite ein, um sie aus dem Skript nutzen zu können.
    Wird nicht bei jedem Seitenaufruf neu geladen sondern unterliegt den Caching-Regeln des Browsers.
    (Gut für Bilder, die man in der Seite anzeigen möchte, oder neue CSS Dateien, ...)
    -> Preloads resources that can by accessed via `GM_getResourceURL(<uniqueId>)` and `GM_getResourceText(<uniqueId>)` by the script.

*/

// Import
import {
    addOwnStyle,
    domainCheck,
    urlRegex,
    urlRegex_presets,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function() {});` gewrappt.
(function() {

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // GitLab: Issue Detail Page
        urlRegex_presets.gitlabIssueDetailPage(),

        // Regex Aufbau der URL, hier "Task List":  https://app.asana.com/<kp>/<boardId>/list
        //                                    z.B:  https://app.asana.com/0/1146334911267339/list
        new urlRegex(/^https:\/\/app\.asana\.com\/([^\/]+)\/(\d+)\/list$/, ["kp", "boardId"]),

        // Regex Aufbau der URL, hier "Task Detail":  https://app.asana.com/<kp>/<boardId>/<taskId>
        //                                      z.B:  https://app.asana.com/0/1146334911267339/1157731227732609
        new urlRegex(/^https:\/\/app\.asana\.com\/([^\/]+)\/(\d+)\/(\d+)$/, ["kp", "boardId", "taskId"]),

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let kp = domainCheckObj.getVariable("kp");
    let boardId = domainCheckObj.getVariable("boardId");
    let taskId = domainCheckObj.getVariable("taskId");

    // Eigenen Style hinzufügen
    addOwnStyle(`<style type='text/css'>
           * {
               background-color: red ! important;
           }
        </style>`);

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(function() {

        // Jetzt kann gearbeitet werden
        // ...

        // Test on what site we are currently on
        if(location.href.match(urlRegex_presets.gitlabIssueDetailPage().regex))
            return;
        else if(location.href.match(urlRegex_presets.gitlabIssueDetailPage().regex))
            return;

        // Beispiel für Regex mit Gruppen in JavaScript
        // Hier zerlegen in: `<symbol><number>[ (<info>)]`
        // const regex = /^(?<symbol>#|!)(?<number>\d+)(?<info> \([^\)]+\))?$/g; // Regex Gruppen mit Namen gehen leider in JavaScript nicht, daher hier Workaround, denn ...
        const regex = /^(#|!)(\d+)( \([^\)]+\))?$/g; // ... statt hier die Gruppen zu benennen ...
        let [, symbol, number, info] = regex.exec(someVariable) || []; // ... werden die Gruppen nachträglich benannt als Variablen abgespeichert
        // ab hier sind die Variablen `symbol`, `number` und `info` verfügbar
        if(!someVariable.match(regex)) // Zusätzlich kann man auf diese Weise prüfen, ob der Regex Aufbau als ganzes eingehalten wird (ohne Gruppen zu nutzen)
            return;

    });

})();
